import React, { Component } from "react";

export default class itemGlasses extends Component {
  render() {
    console.log("item props", this.props.item);
    let { url } = this.props.item;
    return (
      <div
        className="py-4 d-flex justify-content-center my-5"
        id="showGlasses"
        style={{ background: "grey", position: "relative", bottom: "-500px" }}
      >
        <div className="row p-2" style={{}}>
          <div className="col-2">
            <button
              className="w-120 my-2"
              onClick={() => {
                this.props.handleChangeDetail(this.props.item);
              }}
            >
              <img className="w-120" src={url}></img>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
