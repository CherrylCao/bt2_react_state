import React, { Component } from "react";
import { glassesArr } from "./data";
import ListGlass from "./ListGlass";
import Detail from "./Detail";

export default class Ex_Glasses extends Component {
  state = {
    glassesArr: glassesArr,
    detail: glassesArr[0],
  };

  handleChangeDetail = (glasses) => {
    this.setState({ detail: glasses });
  };

  render() {
    return (
      <div>
        <h1 className="text-center py-5">TRY GLASSES APP ONLINE</h1>
        <div className="py-5">
          <div className="d-flex justify-content-center w-25 mx-auto position-relative ">
            <div
              className="img w-50 position-absolute "
              style={{ left: "100px", top: "-110px", zIndex: "3" }}
            >
              <img className="w-100  " src="./glassesImage/model.jpg" alt="" />
            </div>
            <div
              style={{
                display: this.state.display,
                position: "absolute",
                top: "-50px",
                left: "139px",
                backgroundImage: `url(${this.state.backgroundImage})`,
                backgroundSize: "contain",
                backgroundRepeat: "no-repeat",
                width: "30%",
                height: "15vh",
                zIndex: "5",
              }}
            ></div>
          </div>
          <Detail data={this.state.detail} />
          <ListGlass
            style={{
              width: "50%",
            }}
            handleChangeDetail={this.handleChangeDetail}
            list={this.state.glassesArr}
          />
        </div>
      </div>
    );
  }
}
