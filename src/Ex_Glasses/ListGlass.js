import React, { Component } from "react";
import ItemGlasses from "./itemGlasses";

export default class ListGlass extends Component {
  renderList = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemGlasses
          handleChangeDetail={this.props.handleChangeDetail}
          item={item}
          key={index}
        />
      );
    });
  };
  render() {
    console.log(this.props);
    return (
      <div className="py-4 d-flex justify-content-center my-5 ">
        {this.renderList()}
      </div>
    );
  }
}
