import React, { Component } from "react";

export default class Detail extends Component {
  render() {
    let { id, price, name, url, des } = this.props.data;
    return (
      <div>
        <div
          style={{
            display: this.props.display,
            position: "absolute",
            bottom: "-22px",
            left: "103px",
            width: "188px",
            zIndex: "5",
            fontSize: "10px",
          }}
        >
          <img
            style={{
              display: this.props.display,
              position: "absolute",
              zindex: "6",
              left: "1610px",
              bottom: "1520px",
              width: "300px",
            }}
            src={url}
            alt=""
          />
          <div
            style={{
              position: "absolute",
              width: "450px",
              left: "1550px",
              bottom: "1200px",
              fontSize: "30px",
              background: "rgba(224,224,224,0.3)",
            }}
          >
            <h5 className="text-primary">{name}</h5>
            <p className="mb-0 text-white" style={{}}>
              Light pink square lenses define these sunglasses, ending with
              amother of pearl effect tip.{" "}
            </p>
          </div>
        </div>
      </div>
    );
  }
}
