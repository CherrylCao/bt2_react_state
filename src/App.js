import Ex_Glasses from "./Ex_Glasses/Ex_Glasses";

function App() {
  return (
    <div
      className="App"
      style={{
        backgroundImage: "url(./glassesImage/background.jpg)",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <Ex_Glasses />
    </div>
  );
}

export default App;
